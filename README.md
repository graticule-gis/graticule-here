# GRATICULE #



### What is GRATICULE? ###

**GRATICULE** (**G**eographic **R**aster Data **A**nalysis & **T**actical **I**nfrastructure **C**oordination **U**tility for **L**ongevity of the **E**nvironment) 
is a **GIS ( Geographic Information System)** app that uses **HERE Data Layers** and **HERE Location Services**.